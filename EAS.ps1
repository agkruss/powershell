### BEGINNING OF SCRIPT
 
### Updated cmdlet's http://go.microsoft.com/fwlink/p/?LinkId=254711 MobileDevice rather than ActiveSyncDevice
######
#
# HTML Formatting Section
# Thanks to Paul Cunningham at http://exchangeserverpro.com/
#
#######
#
# 
#
######
$style = "<style>BODY{font-family: Arial; font-size: 10pt;}"
$style = $style + "TABLE{border: 1px solid black; border-collapse: collapse;}"
$style = $style + "TH{border: 1px solid black; background: #dddddd; padding: 5px; }"
$style = $style + "TD{border: 1px solid black; padding: 5px; }"
$style = $style + "</style>"

 
####  Get Mailbox
 
$EASDevices = ""
$AllEASDevices = @()
 
$EASDevices = ""| select 'User','PrimarySMTPAddress','DeviceType','DeviceModel','DeviceOS', 'LastSyncAttemptTime','LastSuccessSync'
$EasMailboxes = Get-Mailbox -ResultSize unlimited
foreach ($EASUser in $EasMailboxes) {
$EASDevices.user = $EASUser.displayname
$EASDevices.PrimarySMTPAddress = $EASUser.PrimarySMTPAddress.tostring()
    foreach ($EASUserDevices in Get-MobileDevice -Mailbox $EasUser.alias) {
	$EASDeviceStatistics = $EASUserDevices | Get-MobileDeviceStatistics
    $EASDevices.devicetype = $EASUserDevices.devicetype
    $EASDevices.devicemodel = $EASUserDevices.devicemodel
    $EASDevices.deviceos = $EASUserDevices.deviceos
	$EASDevices.lastsyncattempttime = $EASDeviceStatistics.lastsyncattempttime
	$EASDevices.lastsuccesssync = $EASDeviceStatistics.lastsuccesssync
    $AllEASDevices += $EASDevices | select user,primarysmtpaddress,devicetype,devicemodel,deviceos,lastsyncattempttime,lastsuccesssync
    }
    }
$AllEASDevices = $AllEASDevices | sort user
$AllEASDevices
$AllEASDevices | Export-Csv ActiveSyncReport.csv
write-host ("Script Completed.  Results available in ActiveSyncReport.csv")
##END OF SCRIPT